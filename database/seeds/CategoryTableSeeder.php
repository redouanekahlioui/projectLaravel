<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoryTableSeeder extends Seeder {

    /**
     * Run the Categories table seeds.
     *
     * @return void
     */
    public function run() {
        $data = array(
            [
                'name' => 'Carretera',
                'slug' => 'carretera',
                'description' => 'Lorem ipsum dolor sit amet, consectetur '
                . 'adipisicing elit. Tempore, perferendis!',
                'color' => '#440022'
            ],
            [
                'name' => 'Muntanya',
                'slug' => 'mtb',
                'description' => 'Lorem ipsum dolor sit amet, consectetur'
                . ' adipisicing elit. Tempore, perferendis!',
                'color' => '#445500'
            ],
            ///
            [
                'name' => 'Ciclocross',
                'slug' => 'ciclocross',
                'description' => 'Lorem ipsum dolor sit amet, consectetur '
                . 'adipisicing elit. Tempore, perferendis!',
                'color' => '#440022'
            ],
            [
                'name' => 'Infantils',
                'slug' => 'infantils',
                'description' => 'Lorem ipsum dolor sit amet, consectetur '
                . 'adipisicing elit. Tempore, perferendis!',
                'color' => '#440022'
            ],
            
            [
                'name' => 'Contra Rellotge',
                'slug' => 'contraRellotge',
                'description' => 'Lorem ipsum dolor sit amet, consectetur '
                . 'adipisicing elit. Tempore, perferendis!',
                'color' => '#440022'
            ],
            [
                'name' => 'Transportines',
                'slug' => 'transportines',
                'description' => 'Lorem ipsum dolor sit amet, consectetur '
                . 'adipisicing elit. Tempore, perferendis!',
                'color' => '#440022'
            ],
        );
        Category::insert($data);
    }

}
