<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductTableSeeder extends Seeder {

    /**
     * Run the Categories table seeds.
     *
     * @return void
     */
    public function run() {
        $data = array(
            [
                'name' => 'Bici Carretera 1',
                'slug' => 'carretera-1',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 1372.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod141418_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Bici Carretera 2',
                'slug' => 'carretera-2',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 3154.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod148153_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Bici Carretera 3',
                'slug' => 'carretera-3',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 1999.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod141430_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Bici Muntanya 1',
                'slug' => 'mtb-1',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 1249.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod146561_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            [
                'name' => 'Bici Muntanya 2',
                'slug' => 'mtb-2',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 590.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod154507_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            [
                'name' => 'Bici Muntanya 3',
                'slug' => 'mtb-3',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 772.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod141169_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            [
                'name' => 'Bici Carretera 4',
                'slug' => 'carretera-4',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 999.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod141493_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            [
                'name' => 'Bici Muntanya 4',
                'slug' => 'mtb-4',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 1787.00,
                'image' => 'http://chainreactioncycles.scene7.com/is/image/ChainReactionCycles/prod135669_IMGSET?wid=500&hei=505',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            [
                'name' => 'Bici Muntanya 5',
                'slug' => 'mtb-5',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            
            [
                'name' => 'Bici Carretera 5',
                'slug' => 'carretera-5',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 949.95,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod154502_Carbon%20-%20White_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 1
            ],
            
            [
                'name' => 'Bici Muntanya 6',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 2
            ],
            
            //
            
            [
                'name' => 'Bici Ciclocross 1',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 3
            ],
            
            [
                'name' => 'Bici Ciclocross 2',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 3
            ],
            
            [
                'name' => 'Bici Ciclocross 3',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 3
            ],
            
            [
                'name' => 'Bici Ciclocross 4',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 3
            ],
            
            [
                'name' => 'Bici Ciclocross 5',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 3
            ],
            
            //
            [
                'name' => 'Bici Infàntil 1',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            [
                'name' => 'Bici Infàntil 2',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            [
                'name' => 'Bici Infàntil 3',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            [
                'name' => 'Bici Infàntil 4',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            [
                'name' => 'Bici Infàntil 5',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            [
                'name' => 'Bici Infàntil 6',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 4
            ],
            
            
            
            [
                'name' => 'Bici Contara Rellotge 1',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Contara Rellotge 2',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Contara Rellotge 3',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Contara Rellotge 4',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Contara Rellotge 5',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Contara Rellotge 6',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 5
            ],
            
            [
                'name' => 'Bici Transportines 1',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 2',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 3',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 4',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 5',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 6',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
            [
                'name' => 'Bici Transportines 7',
                'slug' => 'mtb-6',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus repellendus doloribus molestias odio nisi! Aspernatur eos saepe veniam quibusdam totam.',
                'extract' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'price' => 174.99,
                'image' => 'http://media.chainreactioncycles.com/is/image/ChainReactionCycles/template_sample?$detail$&$id=prod164337_Green-Black_NE_01&$offerflag=Clearance&$offerhide=0&$promoflag=PayPalCredit&$promohide=0&locale=es',
                'visible' => 1,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'category_id' => 6
            ],
        );
        Product::insert($data);
    }

}
