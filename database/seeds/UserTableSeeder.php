<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'name' => 'Redouane',
                'last_name' => 'Kahlioui',
                'email' => 'redouanekahlioui@gmail.com',
                'user' => 'rkahlioui',
                'password' => \Hash::make('1559'),                
                'type' => 'admin',
                'active' => 1,
                'address' => 'Carrer Pericas 7 / 8 Manlleu Bacelona España',
                'created_at' => new DateTime,
                'updated_at' => new DateTime

            ],
            
            [
                'name' => 'Usuari1',
                'last_name' => 'Usuari1',
                'email' => 'kahlioui@gmail.com',
                'user' => 'usuari1',
                'password' => \Hash::make('1559'),                                
                'type' => 'user',
                'active' => 1,
                'address' => 'Carrer Manlleu 7 / 8 Torelló Bacelona España',
                'created_at' => new DateTime,
                'updated_at' => new DateTime                
            ],
            
            [
                'name' => 'Usuari2',
                'last_name' => 'Usuari2',
                'email' => 'redouane@javajan.com',
                'user' => 'usuari2',
                'password' => \Hash::make('1559'),                                
                'type' => 'user',
                'active' => 1,
                'address' => 'Avinguda Roma 7 / 8 Manlleu Bacelona España',
                'created_at' => new DateTime,
                'updated_at' => new DateTime
              
            ]
        );
        User::insert($data);
    }
}
