<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers;
use DummyFullModelClass;
use App\lain;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Orders;
use App\OrderItems;
//use Payme\Faker\Provider\nl_BE;

class PaypalController extends Controller
{
    private $_api_context;
    
    public function __construct(){
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index(lain $lain)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function create(lain $lain)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, lain $lain)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function show(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }
    
    public function postPayment(){
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $items = array();
        $subtotal = 0;
        $cart = \Session::get('cart');
        $currency = 'EUR';
        foreach ($cart as $product){
            $item = new Item();
            $item->setName($product->name)
            ->setCurrency($currency)
            ->setDescription($product->extract)
            ->setQuantity($product->quantity)
            ->setPrice($product->price);
            $items[] = $item;
            $subtotal += $product->quantity * $product->price;
        }
        $item_list = new ItemList();
        $item_list->setItems($items);
        $details = new Details();
        $details->setSubtotal($subtotal)->setShipping(100);
        $total = $subtotal + 100;
        $amount = new Amount();
        $amount->setCurrency($currency)
                ->setTotal($total)
                ->setDetails($details);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription("Comanda de prova de Botiga Laravel");
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(\URL::route('payment.status'))
                      ->setCancelUrl(\URL::route('payment.status'));
        $payment = new Payment();
        $payment->setIntent('sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if(\Config::get('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getMessage(), true);
                exit;
            } else {
                die('ERROR!!! Quelcom ha eixit malament');
            }
        }
        
        foreach ($payment->getLinks()as $link){
            if($link->getRel() == 'approval_url'){
                $redirect_url = $link->getHref();
                break;
            }
        }
        \Session::put('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)){
            return \Redirect::away($redirect_url);
        }
        return \Redirect::route('cart-show')
                ->with('error', 'Error Desconegut');
    }
    
    public function getPaymentStatus(Request $request){
        $payment_id = \Session::get('paypal_payment_id');
        \Session::forget('paypal_payment_id');
        $payerId = $request->get('PayerID');
        $token = $request->get('token');
        //if(empty($request->get('PayerID')) || empty($request->get('token')))
        if(empty($payerId) || empty($token)){
            return \Redirect::route('home')
                    ->with('message', 'Ha hagut un problema al l`hora de pagar amb paypal');
        }
        
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        
        if($result->getState() === 'approved'){
            $this->saveOrder(\Session::get('cart'));
            \Session::forget('cart');
            return \Redirect::route('home')
                    ->with('message', 'Compra Realitzada Correctament');
        }
        
        return \Redirect::route('home')
                ->with('message', 'Compra ha estat cancel·lada');
        
    }
    
    private function saveOrder($cart){
        $subtotal = 0;
        foreach ($cart as $item){
            $subtotal += $item->price * $item->quantity;
        }
        $order = Orders::create([
            'subtotal' => $subtotal,
            'shipping' => 100,
            'user_id' => \Auth::user()->id
        ]);
        
        foreach ($cart as $item){
            $this->saveOrderItem($item, $order->id);
        }
    }
    
    private function saveOrderItem($item, $order_id){
        OrderItems::create([
            'quantity' => $item->quantity,
            'price' => $item->price,
            'product_id' => $item->id,
            'order_id' => $order_id
        ]);
    }
}
