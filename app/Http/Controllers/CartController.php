<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller {

    function __construct() {
        /* Si no exixteix una sessio de tipus cart */
        if (!\Session::has('cart')) {
            \Session::put('cart', Array());
        }
    }

    public function show() {

        $cart = \Session::get('cart');
        $total = $this->total();
        return view('store.cart', compact('cart', 'total'));
    }

    /* Afegir nous productes */

    public function add(Product $product) {
        $cart = \Session::get('cart');
        $product->quantity += 1;
        $cart[$product->slug] = $product;
        \Session::put('cart', $cart);
        //despres canviem no cal ficar la ruta per no ser pesat
        return redirect()->route('cart-show');
    }

    /* Mostrar i llegir nous productes */




    /* Actulitza la cistella de compres */

    public function update(Product $product, $quantity) {
        $cart = \Session::get('cart');
        $cart[$product->slug]->quantity = $quantity;
        \Session::put('cart', $cart);
        return redirect()->route('cart-show');
    }

    /* Esborra un producte i esborra la cistella */

    public function delete(Product $product) {
        $cart = \Session::get('cart');
        unset($cart[$product->slug]);
        \Session::put('cart', $cart);

        return redirect()->route('cart-show');
    }

    public function forget() {
        \Session::forget('cart');
        return redirect()->route('cart-show');
    }

    /* Calculs de total */
    
    private function total(){
        $total = 0;
        $cart = \Session::get('cart');
        foreach($cart as $item){
            $total += $item->quantity * $item->price;
        }
        return $total;
    }
    
    public function OrderDetail(){
        if(count(\Session::get('cart')) <= 0){
            return redirect()->route('home');
        }else {
            $cart = \Session::get('cart');
            $total = $this->total();
            return view('store.order-detail', compact('cart', 'total'));
        }
    }
}
