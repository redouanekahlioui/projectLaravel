<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'orders';
    protected $fillable = ['subtotal', 'shipping', 'user_id'];
    public $timestamps = true;
    
//    public function crear(){
//        $article = new Article();
//        $article->nom_article = "Aquest és el títol";
//        $article->descripcio_article = "Aquesta és la descripció";
//        $article->save();
//        echo "creat article amb id: " . $article->id;
//    }
}
