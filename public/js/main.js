/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#demo').pinterest_grid({
        no_columns: 4,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
    $(".btn-update-item").on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var href = $(this).data('href');
        var quantity = $("#product_" + id).val();
        window.location.href = href + "/" + quantity;
    });
});

