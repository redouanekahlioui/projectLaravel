<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand main­title" href="#">Botiga WEB</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <p class="band navbar­text" href="#"><i class="fa da-dashboard"></i> Backend Botiga WEB</p>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Categories</a></li>
                <li><a href="#">Productes</a></li>
                <li><a href="#">Comandes</a></li>
                <li><a href="#">Usuaris</a></li>
                
                @include('store.partials.menu-user')
            </ul>
        </div>
    </div>
</nav>
