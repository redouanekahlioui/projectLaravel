@extends ('store.template')
@section ('content')

<div class="table-cart">
    @if (count($cart))
    <p>
        <a href="{{route('cart-trash')}}" class="btn btn-danger">Buidar Cistella <i class="fa fa-trash"></i></a> 
    </p>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Imatge</th>
                    <th>Quantitat</th>
                    <th>Preu Unitat</th>
                    <th>Quantitat</th>
                    <th>Subtotal</th>
                    <th>Treure</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cart as $item)
                <tr>
                    <td><img alt="{{$item->name}}" src="{{$item->image}}"</td>
                    <td>{{$item->name}}</td>
                    <td>{{number_format($item->price, 2)}} €</td>
                    <td>
                        <input type="number" 
                            min="1" 
                            max="100" 
                            value="{{$item->quantity}}" 
                            id="product_{{$item->id}}">
                        
                        <a href="#" 
                           class="btn btn-warning btn-update-item" 
                           data-href="{{route('cart-update', $item->slug)}}"
                           data-id="{{$item->id }}">
                            <i 
                                class="fa fa-refresh">
                            </i>
                        </a>
                    </td>
                    <td>{{number_format($item->price * $item->quantity, 2)}} €</td>
                    <td>
                        <a href="{{route('cart-delete', $item->slug)}}" class="btn btn-danger">
                            <i class="fa fa-remove"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <h3>
            <span class="label label-success">
                Total: {{number_format($total, 2)}}
            </span>
        </h3>
    </div>
    @else
    <h3><span class="label label-danger">La Cistella Està Buida</span></h3>
    @endif
</div>

<p>
    <a class="btn btn-info" href="{{route ('home')}}"> Tonar Pagina Inici</a>
    <a href="{{route('order-detail')}}" class="btn btn­primary"><i class="fa fa­chevron­circle­right"></i> Continuar
    </a>
</p>
@stop