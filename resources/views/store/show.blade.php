@extends ('store.template')

@section ('content')


<h1>Detall del Producte</h1>

<div class="product-block">
    <img src="{{$product->image}}" width="300">    
</div>
<div class="product-block">
    <h3>{{$product->name}}</h3><hr>
    <div class="product-info">
        <p>{{$product->description}}</p>
        <p>Preu: {{number_format($product->price, 2)}} €</p>
        <p>
            <a class="btn btn-warning btn-block" 
               href="{{route('cart-add', $product->slug)}}">
                Comprar<i class="fa fa-cart-plus fa-x2"></i></a>
        </p>
    </div>
</div>

<p><a class="btn btn-info" href="{{route ('home')}}">Tonar Pagina Inici</a></p>
@stop