@extends ('store.template')
@section ('content')
<section id="demo" class="contenidor_productes">
    @foreach ($products as $product)
    <article class="white-panel">
        <img alt="producte" src="{{$product->image}}" alt="50">
        <h3>{{$product->name}}</h3>
        <div class="product-info">
            <p>{{$product->extract}}</p>
            <p>Preu: {{number_format($product->price,2)}} €</p>
            <p>
                <a class ="btn btn-warning" href="{{route('cart-add', $product->slug)}}">
                    <i class="fa fa-cart-plus"></i>Comprar</a>
                    <a class="btn btn-info"href="{{route('product-detail', $product->slug)}}">Detall</a> 
            </p>
        </div>
    </article>
    @endforeach
</section>

@stop
