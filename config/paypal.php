<?php
return array(
    // set your paypal credential
    'client_id' => 'AerrM2JTqXyvZsxoYXv7jGrHr4Yzf1DRU4EwC7Sf5muPQuNQn67sKr3wJ0tbtFejYsvAvTMrfUcbkXKu',
    'secret' => 'EHGBLvrC6FP8n5yuFVjAi-dZLNpNhOwu-VtLhlBZZvpVuHlNsVBt7gvo0q126NRG7dI8QnpxNs_Q5RIu',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);